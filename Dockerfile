FROM ubuntu:20.04

RUN apt-get update && \
    apt-get install -y screen libjansson4 wget cpulimit
    
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
CMD /entrypoint.sh